import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IllnessComponent } from './illness.component';
import { PatientData } from '../components/survey/patientData';

describe('IllnessComponent', () => {
  let component: IllnessComponent;
  let fixture: ComponentFixture<IllnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IllnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IllnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return result from api call to illnesses', () => {
    var result = component.getIllnesses();
    expect(result!=undefined).toBeTruthy();
  });

  it('patient illness should be empty before click', async() => {
    expect(PatientData.illness == undefined).toBeTruthy();
  })

  it('patient illness should be tester after clicking on testerillness', async() => {
    var illness = {
      id: 1,
      name: "testerillness"
    };
    await component.onIllnessSelect(illness);
    expect(PatientData.illness == "testerillness").toBeTruthy();
  })
});
