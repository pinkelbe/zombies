import { Component, OnInit, Input } from '@angular/core';
import { Illness } from '../components/survey/illness'
import { PatientData } from '../components/survey/patientData';
import { LevelOfPainComponent } from '../level-of-pain/level-of-pain.component'

var db;
var illnessList: Illness[] = [];

@Component({
  selector: 'app-illness',
  templateUrl: './illness.component.html',
  styleUrls: ['./illness.component.scss']
})
export class IllnessComponent implements OnInit {

  @Input() illness: Illness;
  illnessOutput = illnessList;
  selectedIllness: Illness;

  constructor() { }

  ngOnInit(): void {
    this.populateIllnesses();
  }

  public async populateIllnesses(){
    var body;
    body = await this.getIllnesses();
    for(var i = 0; i < body.length; i++) {
      var data = { id: body[i].illness.id, name: body[i].illness.name }
      illnessList.push(data);
    }
  }

  public async getIllnesses() {
    const response = await fetch('http://dmmw-api.australiaeast.cloudapp.azure.com:8080/illnesses');
    const body = await response.json();
    var illnessArray = body._embedded.illnesses;
    return illnessArray;
  }

  onIllnessSelect(illness: Illness): void {
    this.selectedIllness = illness;
    PatientData.illness = this.selectedIllness.name;
    var previous = document.getElementsByClassName('illness-select');
    for (var i = 0; i<previous.length; i++) { 
      const displayChanger = previous[i] as HTMLElement; 
      displayChanger.style.display = "none"; 
    }
    var next = document.getElementsByClassName('level-of-pain')
    for (var i = 0; i<next.length; i++) { 
      const displayChanger = next[i] as HTMLElement; 
      displayChanger.style.display = "unset"; 
    }
  }

}

