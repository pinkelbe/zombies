export interface Hospital {
    id: number;
    name: string;
    LOPCurrentWaitingTime: number;
}