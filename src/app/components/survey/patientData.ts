export class PatientData {
    static id: number;
    static fullName: string;
    static phoneNumber: string;
    static illness: string;
    static levelOfPain: number;
    static hospitalOfChoice: string;
}