import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

var db;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit():void {
    db = this.initializeFirebase();
  }

  public initializeFirebase(){
    try{
      firebase.initializeApp({
        apiKey: "AIzaSyDY8fvxFg3mV3Bze3PkgDx9rUpYlmo3f4Q",
        authDomain: "zombie-953b7.firebaseapp.com",
        projectId: "zombie-953b7"
      })
    }catch (err) {
      if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error raised', err.stack)
      }
    }
    return firebase.firestore();
  }

}
