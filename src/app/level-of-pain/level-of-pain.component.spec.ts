import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PatientData } from '../components/survey/patientData';
import { LevelOfPainComponent } from './level-of-pain.component';

describe('LevelOfPainComponent', () => {
  let component: LevelOfPainComponent;
  let fixture: ComponentFixture<LevelOfPainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevelOfPainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelOfPainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('patient level of pain should be empty', async() => {
    expect(PatientData.levelOfPain == undefined).toBeTruthy();
  })

  it('patient level of pain should not be 3 after clicking on 3', async() => {
    component.onLOPSelect(3);
    expect(PatientData.levelOfPain == 3).toBeTruthy();
  })
  
  it('patient id should be empty', async() => {
    expect(PatientData.id == undefined).toBeTruthy();
  })
  
  it('patient hospital of choice should be empty', async() => {
    expect(PatientData.hospitalOfChoice == undefined).toBeTruthy();
  })

  it('should return result from api call to hospitals', () => {
    var result = component.getHospitals();
    expect(result!=undefined).toBeTruthy();
  });

  it('patient hospital to be hospitaltest and id should not be undefined after click on hospitaltest', async() => {
    var hospital = {
      id: 1,
      name: 'hospitaltest',
      LOPCurrentWaitingTime: 100
    };
    try{
      await component.onHospitalSelect(hospital);
    }catch (err){
      console.log("firebase not tested in this test");
    }
    expect(PatientData.hospitalOfChoice == "hospitaltest").toBeTruthy();
    expect(PatientData.id != undefined).toBeTruthy();
  })
});
