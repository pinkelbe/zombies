import { Component, OnInit, Input } from '@angular/core';
import * as firebase from 'firebase';
import { Hospital } from '../components/survey/hospital'
import { PatientData } from '../components/survey/patientData';
import { WelcomeComponent } from '../welcome/welcome.component'

var db;
var hospitalList: Hospital[] = [];

@Component({
  selector: 'app-level-of-pain',
  templateUrl: './level-of-pain.component.html',
  styleUrls: ['./level-of-pain.component.scss']
})
export class LevelOfPainComponent implements OnInit {
  hospitalOutput = hospitalList;
  selectedLOP: number;
  selectedHospital: Hospital;
  
  constructor() { }

  ngOnInit(): void {
    let firebaseAccess = new WelcomeComponent;
    db = firebaseAccess.initializeFirebase();
    db = firebase.firestore();
  }


  onLOPSelect(LOP: number): void {
    this.selectedLOP = LOP;
    PatientData.levelOfPain = this.selectedLOP;
    var previous = document.getElementsByClassName('level-of-pain');
    for (var i = 0; i<previous.length; i++) { 
      const displayChanger = previous[i] as HTMLElement; 
      displayChanger.style.display = "none"; 
    }
    this.populateHospitals(LOP);
    var next = document.getElementsByClassName('hospitals')
    for (var i = 0; i<next.length; i++) { 
      const displayChanger = next[i] as HTMLElement; 
      displayChanger.style.display = "unset"; 
    }
  }
  
  async onHospitalSelect(hospital: Hospital){
    this.selectedHospital = hospital;
    PatientData.hospitalOfChoice = this.selectedHospital.name;
    var previous = document.getElementsByClassName('hospitals');
    for (var i = 0; i<previous.length; i++) { 
      const displayChanger = previous[i] as HTMLElement; 
      displayChanger.style.display = "none"; 
    }

    await db.collection("patientList").get().then(function(querySnapshot) {      
      if (querySnapshot.size==undefined){
        PatientData.id = 0;
      }else{
        PatientData.id = querySnapshot.size;
      }
    });

    var key = PatientData.id.toString();

    var data = {
      id: PatientData.id,
      name: PatientData.fullName,
      phoneNumber: PatientData.phoneNumber,
      illness: PatientData.illness,
      levelOfPain: PatientData.levelOfPain,
      hospitalOfChoice: PatientData.hospitalOfChoice
    };
    
    await db.collection('patientList').doc(key).set(data)
    .catch(function(error) {
      console.error("Error adding document: ", error);
    });

    var next = document.getElementsByClassName('thankyou')
    for (var i = 0; i<next.length; i++) { 
      const displayChanger = next[i] as HTMLElement; 
      displayChanger.style.display = "unset";
    }
  }

  public async populateHospitals(LOP){
    var body = await this.getHospitals();
    for(var i = 0; i < body.length; i++) {
      var CWT = body[i].waitingList[LOP].averageProcessTime * body[i].waitingList[LOP].patientCount;
      var data = { id: body[i].id, name: body[i].name, LOPCurrentWaitingTime: CWT}
      hospitalList.push(data);
    }

    hospitalList.sort((a, b) => {
      return a.LOPCurrentWaitingTime - b.LOPCurrentWaitingTime;
    });
  }

  public async getHospitals() {
    const response = await fetch('http://dmmw-api.australiaeast.cloudapp.azure.com:8080/hospitals');
    const body = await response.json();
    return body._embedded.hospitals;
  }
}
