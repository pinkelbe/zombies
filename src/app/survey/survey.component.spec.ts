import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SurveyComponent } from './survey.component';
import { PatientData } from '../components/survey/patientData';

describe('SurveyComponent', async() => {
  let component: SurveyComponent;
  let fixture: ComponentFixture<SurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async() => {
    expect(component).toBeTruthy();
  });

  it('patient name should be empty before submit', async() => {
    expect(PatientData.fullName == undefined).toBeTruthy();
  })

  it('patient phone number should be empty before submit', async() => {
    expect(PatientData.phoneNumber == undefined).toBeTruthy();
  })

  it('form should be valid when submitted', async() => {
    component.onSubmitSelect();
    expect(component.submitTester).toBeTruthy();
  })

  it('patient name should not be empty when submitted', async() => {
    await component.onSubmitSelect();
    expect(PatientData.fullName != undefined).toBeTruthy();
  })

  it('patient phone number should not be empty when submitted', async() => {
    await component.onSubmitSelect();
    expect(PatientData.phoneNumber != undefined).toBeTruthy();
  })
  
});