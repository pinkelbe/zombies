import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { PatientData } from '../components/survey/patientData';
import {} from 'googlemaps';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit, AfterViewInit {
  @ViewChild('mapWrapper') mapElement: any;
  map: google.maps.Map;
  constructor() { }

  ngAfterViewInit() {
    this.initializeMap();
  }

  initializeMap() {
    const lngLat = new google.maps.LatLng(-33.9172, 151.2383);
    const mapOptions: google.maps.MapOptions = {
      center: lngLat,
      zoom: 16,
      fullscreenControl: false,
      mapTypeControl: false,
      streetViewControl: false
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

  ngOnInit():void {
    this.submitTester();
  }

  submitTester(){
    var form = document.querySelector('form');
    var inputs = document.querySelectorAll('input');
    var submitButton = document.querySelector('input[type="button"]');
    form.addEventListener('keyup', function(e) {
      var disabled = false;
      inputs.forEach(function(input, index) {
        if (input.value === '' || !input.value.replace(/\s/g, '').length) {
            disabled = true;
        }
      })
      if (disabled) {
        submitButton.setAttribute('disabled', 'disabled');
      } else {
        submitButton.removeAttribute('disabled');
      }
    })
  }

  onSubmitSelect(): void {
    PatientData.fullName = (<HTMLInputElement>document.getElementById('fname')).value;
    PatientData.phoneNumber = (<HTMLInputElement>document.getElementById('pnumber')).value;

    var previous = document.getElementsByClassName('inputfield');
    for (var i = 0; i<previous.length; i++) { 
      const displayChanger = previous[i] as HTMLElement; 
      displayChanger.style.display = "none"; 
    }
    var next = document.getElementsByClassName('illness-select')
    for (var i = 0; i<next.length; i++) { 
      const displayChanger = next[i] as HTMLElement; 
      displayChanger.style.display = "unset"; 
    }
  }

}
