import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component'
import { SurveyComponent } from './survey/survey.component'

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: 'survey',
    component: SurveyComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
