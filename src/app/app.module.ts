import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SurveyComponent } from './survey/survey.component';
import { IllnessComponent } from './illness/illness.component';
import { LevelOfPainComponent } from './level-of-pain/level-of-pain.component';
@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    SurveyComponent,
    IllnessComponent,
    LevelOfPainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
